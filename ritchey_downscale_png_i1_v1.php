<?php
#Name:Ritchey Downscale PNG i1 v1
#Description:Downscale a PNG by 1 position (e.g., 16x16 becomes 8x8). Return path to new PNG on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. 
#Arguments:'source' (required) is a path to a PNG to scale. 'destination' (required) is a path for where to save the scaled PNG. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):source:file:required,destination:path:required,display_errors:bool:optional
#Content:
if (function_exists('ritchey_downscale_png_i1_v1') === FALSE){
function ritchey_downscale_png_i1_v1($source, $destination, $display_errors = NULL){
	$errors = array();
	$progress = '';
	if (@is_file($source) === FALSE){
		$errors[] = "source";
	}
	if (@isset($destination) === FALSE){
		$errors[] = "destination";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Extract the colour of each pixel, delete every second horizontal line of pixels, delete every second pixel of each remaining horizontal line of pixels, and save to a new file]
	if (@empty($errors) === TRUE){
		###Import PNG
		$png = @file_get_contents($source);
		###Convert PNG data to image
		$image = @imagecreatefromstring($png);
		###Determine image width and height
		$width = @imagesx($image);
		$height = @imagesy($image);
		###Determine new image width and height
		$new_width = $width / 2;
		$new_width_rounded = round($new_width, 0, PHP_ROUND_HALF_DOWN);
		$new_height = $height / 2;
		$new_height_rounded = round($new_height, 0, PHP_ROUND_HALF_DOWN);
		###Get each pixel color as RGB, and save to an array
		$working_image = array();
		$total_pixels = $width * $height;
		$pixel_x_coordinate = 0;
		$pixel_y_coordinate = 0;
		for ($i = 0; $i < $total_pixels; $i++) {
			$pixel_colour_index = @imagecolorat($image, $pixel_x_coordinate, $pixel_y_coordinate);
			$pixel_colour_rgb = @imagecolorsforindex($image, $pixel_colour_index);
			$working_image[] = "{$pixel_colour_rgb['red']},{$pixel_colour_rgb['green']},{$pixel_colour_rgb['blue']}|";
			$pixel_x_coordinate++;
			if ($pixel_x_coordinate >= $width){
				$pixel_x_coordinate = 0;
				$pixel_y_coordinate++;
			}
		}
		####Delete last horizontal line of pixels, if new height is greater than rounded down new height (this is needed for images which aren't exactly square)
		if ($new_height > $new_height_rounded){
			$working_image = @array_chunk($working_image, $width, true);
			echo "NH:{$new_height}, NHR:{$new_height_rounded}\n";
			array_pop($working_image);
		} else {
			$working_image = @array_chunk($working_image, $width, true);
		}
		###Delete every second horizontal line of pixels
		$check = FALSE;
		foreach ($working_image as &$value) {
			if ($check === FALSE){
				$check = TRUE;
			} else {
				$check = FALSE;
				$value = '';
			}
		}
		unset($value);
		$working_image = array_filter($working_image);
		$temporary_variable = $working_image;
		foreach ($temporary_variable as &$value) {
			###Delete last last pixel of each line of horizontal pixels, if new width is greater than rounded down new width (this is needed for images which aren't exactly square)
			if ($new_width > $new_width_rounded){
				array_pop($value);
				$value = @implode($value);
			} else {
				$value = @implode($value);
			}
		}
		unset($value);
		$working_image = @implode($temporary_variable);
		unset($temporary_variable);
		$working_image = @explode('|', $working_image);
		$working_image = array_filter($working_image);
		foreach ($working_image as &$value) {
			$value = "{$value}|";
		}
		unset($value);
		###Delete every second pixel of each line of horizontal pixels
		$check = FALSE;
		foreach ($working_image as &$value) {
			if ($check === FALSE){
				$check = TRUE;
			} else {
				$check = FALSE;
				$value = '';
			}
		}
		unset($value);
		$working_image = array_filter($working_image);
		###Convert working image to image data
		####Remove '|' from each array value
		foreach ($working_image as &$value) {
			$value = @substr($value, 0, -1);
		}
		unset($value);
		###Create empty image with black background
		$new_width = $new_width_rounded;
		$new_height = $new_height_rounded;
		$new_image = @imagecreatetruecolor($new_width, $new_height);
		$background_color = imagecolorallocate($new_image, 0, 0, 0);
		###Apply each "Colour" value to a pixel moving from left to right starting at the top going to the bottom.
		$pixel_x_coordinate = 0;
		$pixel_y_coordinate = 0;
		foreach ($working_image as &$value) {
			$value = @explode(',', $value);
			$colour = imagecolorallocate($new_image, $value[0], $value[1], $value[2]);
			imagesetpixel($new_image, $pixel_x_coordinate, $pixel_y_coordinate, $colour);
			//echo "WIDTH:{$new_width}, HEIGHT:{$new_height}, X:{$pixel_x_coordinate}, Y:{$pixel_y_coordinate}, COLOUR:{$value[0]},{$value[1]},{$value[2]}\n";
			$pixel_x_coordinate++;
			if ($pixel_x_coordinate >= $new_width){
				$pixel_x_coordinate = 0;
				$pixel_y_coordinate++;
			}
		}
		unset($value);
		###Convert new image data to PNG format
		ob_start();
		imagepng($new_image);
		$new_png = ob_get_contents();
		ob_end_clean();
		###Save new PNG image to new file. Allow existing files to be overwritten.
		file_put_contents($destination, $new_png);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('ritchey_downscale_png_i1_v1_format_error') === FALSE){
				function ritchey_downscale_png_i1_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("ritchey_downscale_png_i1_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $destination;
	} else {
		return FALSE;
	}
}
}
?>